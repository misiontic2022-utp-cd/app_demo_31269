import 'package:flutter/material.dart';

import 'pages/ejercicio.dart';
import 'package:app_demo_31269/pages/pagina1.dart';

void main(List<String> args) {
  runApp(const MaterialApp(
    title: "Hola Mundo",
    // home: DemoPage(),
    /*
    home: EjercicioPage(
      urlImagen:
          "https://publimotos.com/images/2022/06-junio/junio-29/cali/cali-06-calima.jpg",
      nombre: "Lago Calima",
      ubicacion: "Valle del Cauca, Colombia",
      numeroFavoritos: 41,
      descripcion:
          "El Lago Calima es un reservorio de agua artificial con características interesantes, también es uno de los embalses más grandes de América. Este lugar cuenta con un atractivo turístico especial que está enmarcado con hermosas formaciones montañosas, cuyo encanto atrae a miles de turistas cada año.\n\nEl Lago Calima se ubica en el municipio de Calima el Darién en el Departamento del Valle del Cauca en Colombia, en el continente suramericano, este reconocido lago es visitado por cientos de turistas de diferentes nacionalidades cada día y todos quedan fascinados por la belleza de sus verdes paisajes.",
    ),
    */
    home: PaginaUnoPage(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: "Hola Mundo",
      home: EjercicioPage(
        urlImagen:
            "https://publimotos.com/images/2022/06-junio/junio-29/cali/cali-06-calima.jpg",
        nombre: "Laguna Guatavita",
        ubicacion: "Valle del Cauca, Colombia",
        numeroFavoritos: 41,
        descripcion:
            "El Lago Calima es un reservorio de agua artificial con características interesantes, también es uno de los embalses más grandes de América. Este lugar cuenta con un atractivo turístico especial que está enmarcado con hermosas formaciones montañosas, cuyo encanto atrae a miles de turistas cada año.\n\nEl Lago Calima se ubica en el municipio de Calima el Darién en el Departamento del Valle del Cauca en Colombia, en el continente suramericano, este reconocido lago es visitado por cientos de turistas de diferentes nacionalidades cada día y todos quedan fascinados por la belleza de sus verdes paisajes.",
      ),
    );
  }
}
