import 'package:flutter/material.dart';

import 'opcion_accion.dart';

class AccionesWidget extends StatelessWidget {
  const AccionesWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsetsDirectional.fromSTEB(0, 15, 0, 20),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: const [
          OpcionAccion(icono: Icons.call, texto: "Llamar"),
          OpcionAccion(icono: Icons.location_pin, texto: "Ruta"),
          OpcionAccion(icono: Icons.share_rounded, texto: "Compartir"),
        ],
      ),
    );
  }
}
