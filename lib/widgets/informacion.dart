import 'package:app_demo_31269/widgets/favoritos.dart';
import 'package:flutter/material.dart';

class InformationWidget extends StatelessWidget {
  final String nombre;
  final String ubicacion;
  final int numeroFavoritos;

  const InformationWidget(
      {super.key,
      required this.nombre,
      required this.ubicacion,
      required this.numeroFavoritos});

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: const AlignmentDirectional(0, 0.05),
      child: Padding(
        padding: const EdgeInsetsDirectional.fromSTEB(30, 20, 40, 20),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Align(
                    alignment: const AlignmentDirectional(-1, 0),
                    child: Text(
                      nombre,
                      textAlign: TextAlign.start,
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 18),
                    ),
                  ),
                  Align(
                    alignment: const AlignmentDirectional(-1, 0),
                    child: Text(
                      ubicacion,
                      textAlign: TextAlign.start,
                    ),
                  ),
                ],
              ),
            ),
            FavoritosWidget(
              numeroFavoritos: numeroFavoritos,
            )
          ],
        ),
      ),
    );
  }
}
