import 'package:flutter/material.dart';

class FavoritosWidget extends StatefulWidget {
  int numeroFavoritos;

  FavoritosWidget({super.key, required this.numeroFavoritos});

  @override
  State<FavoritosWidget> createState() => _FavoritosWidgetState();
}

class _FavoritosWidgetState extends State<FavoritosWidget> {
  bool _estaMarcado = false;

  void _marcarBoton() {
    setState(() {
      widget.numeroFavoritos += (_estaMarcado ? -1 : 1);
      _estaMarcado = !_estaMarcado;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        IconButton(
          icon: Icon(_estaMarcado ? Icons.star : Icons.star_outline),
          color: const Color(0xFFFF0000),
          onPressed: _marcarBoton,
        ),
        Text("${widget.numeroFavoritos}"),
      ],
    );
  }
}
