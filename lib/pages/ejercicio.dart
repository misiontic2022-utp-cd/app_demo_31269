import 'package:app_demo_31269/widgets/acciones.dart';
import 'package:app_demo_31269/widgets/informacion.dart';
import 'package:flutter/material.dart';

class EjercicioPage extends StatelessWidget {
  final String urlImagen;
  final String nombre;
  final String ubicacion;
  final int numeroFavoritos;
  final String descripcion;

  const EjercicioPage(
      {super.key,
      required this.urlImagen,
      required this.nombre,
      required this.ubicacion,
      required this.numeroFavoritos,
      required this.descripcion});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            const SizedBox(
              height: 60,
            ),
            Image.network(urlImagen),
            InformationWidget(
              nombre: nombre,
              ubicacion: ubicacion,
              numeroFavoritos: numeroFavoritos,
            ),
            const AccionesWidget(),
            Padding(
              padding: const EdgeInsetsDirectional.fromSTEB(20, 0, 20, 0),
              child: Text(
                descripcion,
                textAlign: TextAlign.start,
                maxLines: 30,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
