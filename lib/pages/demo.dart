import 'package:flutter/material.dart';

class DemoPage extends StatelessWidget {
  const DemoPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Column(
            children: const <Text>[
              Text("Pagina 2"),
              Text("Segunda linea"),
            ],
          ),
        ),
      ),
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              children: const <Text>[
                Text("Hola Mundo"),
                Text("Segunda linea"),
                Text("Segunda linea"),
                Text("Segunda linea"),
                Text("Segunda linea"),
                Text("Segunda linea"),
                Text("Segunda linea"),
                Text("Segunda linea"),
                Text("Segunda linea"),
              ],
            ),
            const SizedBox(
              width: 10,
            ),
            Column(
              children: const <Text>[
                Text("Hola Mundo 2"),
                Text("Segunda linea segunda linea"),
                Text("Segunda linea"),
                Text("Segunda linea"),
                Text("Segunda linea"),
                Text("Segunda linea"),
                Text("Segunda linea"),
                Text("Segunda linea"),
                Text("Segunda linea"),
              ],
            ),
            const SizedBox(
              width: 10,
            ),
            Column(
              children: const <Text>[
                Text("Hola Mundo 3"),
                Text("Segunda linea"),
                Text("Segunda linea"),
                Text("Segunda linea"),
                Text("Segunda linea"),
                Text("Segunda linea"),
                Text("Segunda linea"),
                Text("Segunda linea"),
                Text("Segunda linea"),
              ],
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: const Icon(Icons.add),
      ),
    );
  }
}
