import 'package:app_demo_31269/pages/pagina2.dart';
import 'package:flutter/material.dart';

class PaginaUnoPage extends StatelessWidget {
  const PaginaUnoPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Pagina 1"),
      ),
      body: Center(
        child: ElevatedButton(
          child: const Text("Ir a pagina 2"),
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => const PaginaDosPage()));
          },
        ),
      ),
    );
  }
}
